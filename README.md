# Prepare a GPU instance

When you start a GPU instance on AWS or GCP, you will need to install some libraries to customized the enviroment.

At first, you should choose an instance which uses ubuntu 18.04.

#### To install nvidia-driver and cuda driver on an instance with GPU

Version : nvidia-driver 410 and cuda 10.0

Reference : https://askubuntu.com/questions/1077061/how-do-i-install-nvidia-and-cuda-drivers-into-ubuntu
```
sudo rm /etc/apt/sources.list.d/cuda*
sudo apt remove nvidia-cuda-toolkit
sudo apt remove nvidia-*
sudo apt update
sudo apt-key adv --fetch-keys http://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64/7fa2af80.pub 
sudo bash -c 'echo "deb http://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64 /" > /etc/apt/sources.list.d/cuda.list'
sudo apt update
sudo apt install nvidia-driver-410
sudo apt install cuda-10-0
```
#### To install docker-ce

Version : docker-ce:latest

Reference : https://docs.docker.com/install/linux/docker-ce/ubuntu/#upgrade-docker-ce-1
```
sudo apt-get remove docker docker-engine docker.io
sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
sudo apt-get update
sudo apt-get install docker-ce
```
#### To install nvidia-docker 2.0

Version : nvidia-docker 2.0

Reference : https://github.com/nvidia/nvidia-docker/wiki/Installation-(version-2.0)
```
sudo docker volume ls -q -f driver=nvidia-docker | xargs -r -I{} -n1 sudo docker ps -q -a -f volume={} | xargs -r sudo docker rm -f
sudo apt-get purge nvidia-docker
curl -s -L https://nvidia.github.io/nvidia-docker/gpgkey | \
  sudo apt-key add -
distribution=$(. /etc/os-release;echo $ID$VERSION_ID)
curl -s -L https://nvidia.github.io/nvidia-docker/$distribution/nvidia-docker.list | \
  sudo tee /etc/apt/sources.list.d/nvidia-docker.list
sudo apt-get update
sudo apt-get install nvidia-docker2
sudo pkill -SIGHUP dockerd
```
